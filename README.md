# ขั้นตอนการใช้งานเบื้องต้น Kubernetes

## หัวข้อทั้งหมด

- [Docker](#Docker)

- [Kubernetes](#Kubernetes)

- [Helm](#Helm)

## Docker
---
วิธีการติดตั้ง Docker บน Ubuntu เวอร์ชัน 16 เป็นต้นไป

```
$ sudo apt-get update
$ sudo apt-get upgrade
$ sudo apt-get install docker-compose up

```

หลังจากติดตั้งให้เปิด Diractory /Docker ใน path
```
$ cd Docker
```

![image1.png](./image1.png "image1.png")
โดยจะประกอปไปด้วย 

- Dockerfile คือไฟล์ สำหรับ Build images สำหรับ การใช้งาน

ทำการ Create Image โดยพิมคำสั่ง 
```
$ sudo docker build -t <ชื่อ images ที่ต้องการ บิ้ว> . 
```
ทำการทดสอบโดยพิมคำสั่ง
```
$ sudo docker run -n image-test -p 80:80 <ชื่อ images ที่สร้าง> -d
```
เข้าไปที localhost จะพบกลับ service ที่เราได้ทำไว้ เช่น
![image2.png](./image2.png "image2.png")
ทำการลบ docker ที่ run อยู่  
```
$ sudo docker rm -f image-test 
```
เสร็จสิ้นขั้นตอน
## Kubernetes
---
ขั้นตอนการตินตั้ง สำหรับ Ubunto vertion 16 เป็นต้นไป
ตามปกติ Ubunto vertion 16 จะติดตั้ง snap โดย Default
เเต่ถ้าไม่มีจะทำการติดตั้งได้ผ่าน 

* https://snapcraft.io/docs/installing-snap-on-elementary-os

```
sudo snap install microk8s --classic --channel=1.17/stable
sudo usermod -a -G microk8s warun
su - warun
microk8s.status --wait-ready
```
ทำการตรวจสอบการใช่งาน microk8s โดยพิมคำสั่ง

```
microk8s.kubectl get nodes
```
ทดลองการติดตั้งกับ images ที่ได้สร้างใน ขั้นตอนของ Docker
เข้าไปที่ ไฟล์ /Kubernetes/deploy-nodePort.yaml
แก้ไข image ที่จะทำการ deploy เป็น image ของเรา 
จะได้หน้าตาประมาณนี้

![image3.png](./image3.png "image3.png")
ทำการ deploy โดยพิมคำสั่ง 
```
$ microk8s.kubectl create ns test-deploy
$ microk8s.kubectl apply -f /Kubernetes/deploy-nodePort.yaml -n  test-deploy
$ microk8s.kubectl get all -n test-deploy
```

ผลลัพที่ได้คือ

![image4.png](./image4.png "image4.png")

ทำการทดสอบผลลัพโดยการ ไปที่ port localhost:30007 

![image5.png](./image5.png "image5")

ทำการลบ service ที่ Deploy 
```
$ microk8s.kubectl delete -f /Kubernetes/deploy-nodePort.yaml -n  test-deploy
```
เสร็จสิ้นขั้นตอน

## Helm

ติดตั้งการใช่งาน Helm สำหรับ Ubuntu
```
$ curl https://baltocdn.com/helm/signing.asc | sudo $ apt-key add -
$ sudo apt-get install apt-transport-https --yes
$ echo "deb https://baltocdn.com/helm/stable/debian/ all $ main" | sudo tee /etc/apt/sources.list.d/$ helm-stable-debian.list
$ sudo apt-get update
$ sudo apt-get install helm
```

การทดลอง helm ให้เค้าไปที่ diractory Helm หลังจากนันพิมคำสั่ง 
```
$ helm create <ชื่อ Service ที่ต้องการตั้ง> -n  test-deploy
```
ใน diractory จะพบกับชือที่เราตังให้เข้าไปเพือเเก้ไข image ที่ต้องการ Deploy 
cd <ชื่อ Service ที่ต้องการตั้ง>/values.yaml
จะพบกับ ข้อมูล ให้ทำการแก้ไขดังภาพ
![image6.png](./image6.png "image6")
ทำตามขั้นตอน ดังสริป
![image7.png](./image7.png "image7")

เข้าไปที่ 127.0.0.1:8080 
เสร็จสิน 